#include "buddy.h"





int main() {
  size_t poolSize = MAX_SIZE_BUFFER_BUDDY; // Total size of the memory pool (1MB)
  BuddyAllocator *allocator = buddy_allocator_create(poolSize);
  
  // Example usage
  int *smallAllocation = (int *)buddy_allocator_malloc(allocator, 248);
  printBitAtPosition(allocator->bitmap,8193);
  int bit4096 = BitMap_getBit(allocator->bitmap, 4096);
  printf("Bit at position 4096 in the BitMap: %d\n", bit4096);
  int *smallAllocation2 = (int *)buddy_allocator_malloc(allocator, 248);

  
  int *smallAllocation3 = (int *)buddy_allocator_malloc(allocator,248);
  int *smallAllocation4 = (int *)buddy_allocator_malloc(allocator, 248);
  int *smallAllocation5 = (int *)buddy_allocator_malloc(allocator, 248);
  int *smallAllocation6 = (int *)buddy_allocator_malloc(allocator, 128);
  //int bit4096 = BitMap_getBit(allocator->bitmap, 4096);
  //printf("Bit at position 3096 in the BitMap: %d\n", bit4096);
   

  
  if (smallAllocation != NULL && smallAllocation2 != NULL && smallAllocation3 != NULL &&
      smallAllocation4 != NULL && smallAllocation5 != NULL && smallAllocation6 != NULL) {
    *smallAllocation2 = 42;
    *smallAllocation3 = 4;
    *smallAllocation4 = 4;
    *smallAllocation5 = 4;
    *smallAllocation6 = 88;
    
    

    printf("Small allocation: %d\n", *smallAllocation);
    printf("---");
    
    
    printf("Small allocation: %d\n", *smallAllocation2);
    printf("---");
    printf("Small allocation: %d\n", *smallAllocation3);
    printf("---");
    printf("Small allocation: %d\n", *smallAllocation4);
    printf("---");
    printf("Small allocation: %d\n", *smallAllocation5);
    printf("---");
    printf("Small allocation: %d\n", *smallAllocation6);
    buddy_allocator_free(allocator, smallAllocation);
    buddy_allocator_free(allocator, smallAllocation2);
    buddy_allocator_free(allocator, smallAllocation3);
    buddy_allocator_free(allocator, smallAllocation4);
    buddy_allocator_free(allocator, smallAllocation5);
    buddy_allocator_free(allocator, smallAllocation6);
    printBitAtPosition(allocator->bitmap,8193);
    int bit4096 = BitMap_getBit(allocator->bitmap, 4096);
    printf("Bit at position 4096 in the BitMap: %d\n", bit4096);
  }

  int *largeAllocation = (int *)buddy_allocator_malloc(allocator, 5000);
  if (largeAllocation != NULL) {
    largeAllocation[0] = 123;
    printf("Large allocation: %d\n", largeAllocation[0]);
    buddy_allocator_free(allocator, largeAllocation);
  }

  buddy_allocator_destroy(allocator);
//end

  return 0;
}
