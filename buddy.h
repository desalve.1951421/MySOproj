#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#define PAGE_SIZE (4 * 1024) // Assuming page size is 4KB
#define SMALL_ALLOC_SIZE (PAGE_SIZE / 4) // Maximum size for small allocations
#define MAX_SIZE_BUFFER_BUDDY (1024 * 1024) // 1MB
#define NUM_BUDDY_LEVELS 20 // Number of levels in the buddy allocator
#define MAX_MEMSET_ALLOCATIONS 1000

typedef struct {
    void *ptr;
    size_t size;
} MemsetAllocation;

typedef struct {
  uint8_t *buffer; // externally allocated buffer
  int buffer_size;
  int num_bits;
} BitMap;


typedef struct BuddyAllocator {
  BitMap *bitmap; // Bitmap for small allocations
  unsigned char *memory; // Memory pool
  
  MemsetAllocation memset_allocations[MAX_MEMSET_ALLOCATIONS];
  int memset_allocation_count;
    
   size_t pool_size; // Total size of the memory pool
  size_t free_space; // Remaining free space in the memory pool
} BuddyAllocator;

int levelIdx(size_t idx);


int first_idx_from_level(size_t level);


int parentIdx(int idx);

int startIdx(int idx);

int BitMap_get_bytes_from_bits(int bits);

void bitmap_initialization(BitMap *bit_map, int num_bits) ;

void BitMap_setBit(BitMap* bit_map, int bit_num, int status);

int BitMap_getBit(const BitMap *bit_map, int bit_num);

void printBitAtPosition(BitMap *bit_map, int position);

BuddyAllocator *buddy_allocator_create(size_t pool_size);


void buddy_allocator_destroy(BuddyAllocator *allocator) ;


void set_all_children_as_occupied(BitMap* bitmap, size_t level, size_t index);


void set_all_children_as_free(BitMap *bitmap, size_t level, size_t index) ;

int is_allocated_by_buddy_allocator(BuddyAllocator *allocator, void *ptr);

void *buddy_allocator_malloc(BuddyAllocator *allocator, size_t size);

void printval(void*ptr);


void buddy_allocator_free(BuddyAllocator *allocator, void *ptr);









