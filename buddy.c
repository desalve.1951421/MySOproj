#include "buddy.h"


// memset

//ottengo il livello dato l'indice
int levelIdx(size_t idx) {  double logVal = log2(idx);
  int floorVal = (int)floor(logVal);
  int ceilVal = (int)ceil(logVal);
  return (logVal - floorVal) < (ceilVal - logVal) ? floorVal : ceilVal;}


int first_idx_from_level(size_t level){return (int)1<<(level - 1);}//primo indice del livello dato livello

int last_idx_from_level(size_t level){return (int)1<<(level+1) - 1;}//primo indice del livello dato livello

int buddyIdx(int idx) {
  if (idx & 0x1) {
    return idx - 1;
  }
  return idx + 1;
}

int parentIdx(int idx) { return idx / 2; }

int startIdx(int idx) { return (idx - (1 << levelIdx(idx))); }

int BitMap_get_bytes_from_bits(int bits) { return (bits + 7) / 8; }

void bitmap_initialization(BitMap *bit_map, int num_bits) {
  bit_map->num_bits = num_bits;
  bit_map->buffer_size = BitMap_get_bytes_from_bits(num_bits);
  bit_map->buffer = (uint8_t *)malloc(bit_map->buffer_size);
  memset(bit_map->buffer, 0x00, bit_map->buffer_size); // Set all bytes to 0x00
}
//ora imposta il bit corrispondente nel buffer di bit_map su 1. Fa ciò utilizzando l'operazione OR bit a bit |= e uno spostamento a sinistra (<<) di 1 di bit posizioni,
// quindi applica l'OR bit a bit al byte corrispondente nel buffer. Questo ha l'effetto di impostare il bit in quella posizione a 1, mentre gli altri bit nel byte rimangono invariati.
//. Fa ciò utilizzando l'operazione AND bit a bit &= con l'opposto (complemento a 1) di uno spostamento a sinistra (<<) di 1 di bit posizioni, quindi applica l'AND bit a bit al byte corrispondente nel buffer. 
void BitMap_setBit(BitMap* bit_map, int bit_num, int status) {
  int byte = bit_num / 8;
  int bit = bit_num % 8;

  if (status == 1)
    bit_map->buffer[byte] |= (1 << bit);
  else
    bit_map->buffer[byte] &= ~(1 << bit);
}

int BitMap_getBit(const BitMap *bit_map, int bit_num) {
  int byte = bit_num / 8;
  int bit = bit_num % 8;

  return (bit_map->buffer[byte] & (1 << bit)) != 0;
}
void printBitAtPosition(BitMap *bit_map, int position) {
  int bitValue = BitMap_getBit(bit_map, position);
  printf("Bit at position %d in the BitMap: %d\n", position, bitValue);
}


BuddyAllocator *buddy_allocator_create(size_t pool_size) {
  BuddyAllocator *allocator = (BuddyAllocator *)malloc(sizeof(BuddyAllocator));
  allocator->bitmap = (BitMap *)malloc(sizeof(BitMap));
  allocator->memory =
      (unsigned char *)mmap(NULL, pool_size, PROT_READ | PROT_WRITE,
                            MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  bitmap_initialization(allocator->bitmap, MAX_SIZE_BUFFER_BUDDY *8);
   allocator->pool_size = pool_size;
  allocator->free_space = pool_size;
  
  //
   allocator->memset_allocation_count = 0;
  return allocator;
}

void buddy_allocator_destroy(BuddyAllocator *allocator) {
  munmap(allocator->memory, MAX_SIZE_BUFFER_BUDDY);
  free(allocator->bitmap->buffer);
  free(allocator->bitmap);
  free(allocator);
}
void set_all_children_as_occupied(BitMap* bitmap, size_t level, size_t index) {

    if (level == NUM_BUDDY_LEVELS) {
        return; // Terminate recursion when we reach the maximum level
    }

    int left_child_index = (index * 2);
    int right_child_index = (index * 2) + 1;

    BitMap_setBit(bitmap, left_child_index, 1);
    BitMap_setBit(bitmap, right_child_index, 1);

    set_all_children_as_occupied(bitmap, level + 1, left_child_index);
    set_all_children_as_occupied(bitmap, level + 1, right_child_index);

}

void set_all_children_as_free(BitMap *bitmap, size_t level, size_t index) {
    if (level == NUM_BUDDY_LEVELS) {
        return; // Terminate recursion when we reach the maximum level
    }

    int left_child_index = (index * 2);
    int right_child_index = (index * 2) + 1;

    BitMap_setBit(bitmap, left_child_index, 0);
    BitMap_setBit(bitmap, right_child_index, 0);

    set_all_children_as_free(bitmap, level + 1, left_child_index);
    set_all_children_as_free(bitmap, level + 1, right_child_index);
}

int is_allocated_by_buddy_allocator(BuddyAllocator *allocator, void *ptr) {
    uintptr_t ptr_addr = (uintptr_t)ptr;
    uintptr_t pool_start = (uintptr_t)allocator->memory;
    uintptr_t pool_end = pool_start + allocator->pool_size;

    if (ptr_addr >= pool_start && ptr_addr < pool_end) {
        // Il puntatore si trova nel range della memoria allocata dal buddy allocator
        return 1; // Vero, il puntatore è stato allocato dal buddy allocator
    } else {
        return 0; // Falso, il puntatore non è stato allocato dal buddy allocator
    }
}


void *buddy_allocator_malloc(BuddyAllocator *allocator, size_t size) {
  if (size <= SMALL_ALLOC_SIZE) {
    // Use buddy allocator for small allocations
    size_t level_start = NUM_BUDDY_LEVELS - levelIdx(size);//ottengo lv del buddy da controllare
    size_t dim=size;
    size_t i, j;
    if(allocator->free_space>=dim)

    for (i = level_start; i >= 0; i--) {

    
    
    size_t numBlocks = 1 << i;//numeri di blocchi da scorrere
    size_t blockSize = 1<<levelIdx(dim);
    if(i!=level_start){//salgo di dimensione poiche salgo verso radice
		dim*=2;
		blockSize = 1<<levelIdx(dim); //dimensione ogni blocco == 2^20-2^x
	}
    
	  size_t index_start = (1<<i);
	  
	  size_t index_end_ofLevel = index_start+numBlocks;
	  
      
      printf("cerco nel livello %zu :\n", i);
      printf("numero blocchi a quel livello : %zu\n", numBlocks);
      //fino a qui ok
      
      for (j =index_start; j <index_end_ofLevel; j++) {
		  int free=0;
        if (BitMap_getBit(allocator->bitmap, j)==0) {
            BitMap_setBit(allocator->bitmap, j, 1);
            BitMap_setBit(allocator->bitmap, parentIdx(j), 1);
            set_all_children_as_occupied(allocator->bitmap,i,j);
            free=1;
          }
         if(free==1)break;
        }

      
            
		
          void *ptr = &allocator->memory[startIdx(j) * blockSize];
          printf("Allocated %zu bytes from buddy level %zu at position %zu\n",
                 size, i, j);
    

                // Salva l'indice j nell'area di memoria subito dopo ptr con un offset fisso
                size_t *indexPtr = (size_t *)((char *)ptr + 4); // Offset di 4 byte

                // Imposta l'indice j nell'offset fisso
                *indexPtr = j;

                
  
          
          
          allocator->free_space-=size;
          return ptr;
        }
          printf("non ho memoria disponibile\n");
    return NULL; // No available memory in the buddy allocator
      }
    
    

    
   else {
    // Use mmap for large allocations
   if (allocator->memset_allocation_count < MAX_MEMSET_ALLOCATIONS) {
    size_t numPages = (size + PAGE_SIZE - 1) / PAGE_SIZE;
    void *ptr = mmap(NULL, numPages * PAGE_SIZE, PROT_READ | PROT_WRITE,
                     MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    printf("Allocated %zu bytes using mmap\n", size);
    allocator->memset_allocations[allocator->memset_allocation_count].ptr = ptr;
    allocator->memset_allocations[allocator->memset_allocation_count].size = size;
    allocator->memset_allocation_count++;
    return (ptr == MAP_FAILED) ? NULL : ptr;
    
   
  }
	else{
		printf("superato limite memset");
		return NULL;
		}
   }
		
}

void printval(void*ptr){
size_t *indexPtr = (size_t *)((char *)ptr + 4); // Offset di 4 byte
    size_t j = *indexPtr;
printf("%zu",j);
}


void buddy_allocator_free(BuddyAllocator *allocator, void *ptr) {
	if(is_allocated_by_buddy_allocator(allocator,ptr)){
    size_t *indexPtr = (size_t *)((char *)ptr + 4); // Offset di 4 byte
    size_t index = *indexPtr;
    size_t level=levelIdx(index);
    size_t num_blocks = allocator->pool_size / (1 << (NUM_BUDDY_LEVELS - level));
    size_t dime= 1<<(NUM_BUDDY_LEVELS-levelIdx(num_blocks));
    void *originalPtr = (void *)((char *)ptr - 4);

    printf("\n index %zu ",index);
    printf("\n num_blocks%zu ",num_blocks);
     printf("\n dime %zu ",dime);

    if (dime <= SMALL_ALLOC_SIZE) {
        // Freeing small allocations from buddy allocator
        size_t level_start = level;
        size_t dim=dime;
        
        size_t i, j;

        for (i = level_start; i >= 0; i--) {
            size_t numBlocks = 1 << i;
            size_t blockSize = 1 << levelIdx(dim);
            if (i != level_start) {
                dim *= 2;
                blockSize = 1 << levelIdx(dim);
            }

            size_t index_start = (1 << i);
            size_t index_end_ofLevel = index_start + numBlocks;

            

            for (j = index_start; j < index_end_ofLevel; j++) {
                int free = 0;
                if ((uintptr_t)&allocator->memory[startIdx(j) * blockSize] == (uintptr_t)ptr) {
                    BitMap_setBit(allocator->bitmap, j, 0);
                    if(BitMap_getBit(allocator->bitmap,buddyIdx(j))==0)BitMap_setBit(allocator->bitmap,parentIdx(j),0);
                    set_all_children_as_free(allocator->bitmap, i, j);
                    free = 1;
                }
                if (free == 1) break;
            }

            void *allocatedPtr = &allocator->memory[startIdx(j) * blockSize];
            printf("Freed %zu bytes from buddy level %zu at position %zu\n", dim, i, j);
            return;
        }
        printf("Memory not found\n");
       }
    } else {
         for (int i = 0; i < allocator->memset_allocation_count; i++) {
                if (allocator->memset_allocations[i].ptr == ptr) {
                    size_t numPages = (allocator->memset_allocations[i].size + PAGE_SIZE - 1) / PAGE_SIZE;
                    munmap(allocator->memset_allocations[i].ptr, numPages * PAGE_SIZE);
                    printf("Freed %zu bytes using mmap\n", allocator->memset_allocations[i].size);

                    // Rimuovi questa allocazione dall'array
                    for (int j = i; j < allocator->memset_allocation_count - 1; j++) {
                        allocator->memset_allocations[j] = allocator->memset_allocations[j + 1];
                    }
                    allocator->memset_allocation_count--;
                    return;
                }
            }
    }

}

